var x = [];
var y = [];
var previsao = [];
function removerUndefined(array) {
  for (let i = 0; i <= array.length; ++i) {
    if (typeof array[i] === "undefined") {
      return array.splice(i, array.length);
    }
  }
}
function lerExcel() {
  var xlsx = require("xlsx");

  var wb = xlsx.readFile("Panilha AL 30_09_2020.xlsx", { cellDates: true });

  var ws = wb.Sheets["Página1"];

  var data = xlsx.utils.sheet_to_json(ws);

  x = data.map((record) => {
    return record.casosNovos;
  });
  y = data.map((record) => {
    return record.obitosNovos;
  });
  previsao = data.map((record) => {
    return record.validacao;
  });

  removerUndefined(x);
  removerUndefined(y);
  removerUndefined(previsao);

  console.log(x);
  console.log(y);
  console.log(previsao);
}

async function ensinandoML() {
  const tf = require("@tensorflow/tfjs");

  const model = tf.sequential();
  model.add(tf.layers.dense({ units: 1, inputShape: [1] }));

  model.compile({
    loss: "meanSquaredError",
    optimizer: "adam",
  });

  const xs = tf.tensor2d(x, [x.length, 1]);
  const ys = tf.tensor2d(y, [y.length, 1]);

  await model.fit(xs, ys, { epochs: 50000 }).then(() => {
    model.predict(tf.tensor2d(previsao, [previsao.length, 1])).print();
  });
}
lerExcel();
ensinandoML();
